package com.epam.java_basic.calculator;

public class Calculator {

    private static final int ROUND_POWER_BASE = 10;

    private final int precision;

    public Calculator(int precision) {
        this.precision = precision;
    }

    public double add(double a, double b) {
        return toFixed(a + b);
    }

    public double subtract(double a, double b) {
        return toFixed(a - b);
    }

    public double multiply(double a, double b) {
        return toFixed(a * b);
    }

    public double div(double a, double b) {
        return toFixed(a / b);
    }

    private double toFixed(double value) {
        double result = value;
        if (Double.isFinite(value)) {
            result = Math.round(value * Math.pow(ROUND_POWER_BASE, precision)) / Math.pow(ROUND_POWER_BASE, precision);
        }
        return result;
    }
}
